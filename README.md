**REACT-NATIVE-SODIUM**

OVERVIEW & PURPOSE
We have followed some steps to integrate it in our react-native project, by doing the below mentioned subject you will able to configure that steps in our app, after following these steps your app will work like a charm!
Android studio configurations
We will add them in sdk tools 
Add CMAKE
Add LLDB
NDK 
To remove CMakeLists.txt:23
Add this in node_modules/react-native-sodium/android/CMAKELists.txt
Just Replace all code in that file with this one
cmake_minimum_required(VERSION 3.4.1)

set(ARCH_DIR ${ANDROID_ARCH_NAME})

if( ANDROID_ABI STREQUAL "armeabi-v7a")
  set( ARCH_DIR "armv7-a" )
elseif ( ANDROID_ABI STREQUAL "arm64-v8a")
  set( ARCH_DIR "armv8-a" )
elseif ( ANDROID_ABI STREQUAL "x86")
  set ( ARCH_DIR "i686" )
elseif ( ANDROID_ABI STREQUAL "x86_64")
  set ( ARCH_DIR "westmere" )
endif()


add_library(sodium SHARED IMPORTED)
include_directories( ${PROJECT_SOURCE_DIR}/../libsodium/libsodium-android-${ARCH_DIR}/include/ )
set_target_properties( sodium PROPERTIES IMPORTED_LOCATION ${PROJECT_SOURCE_DIR}/../libsodium/libsodium-android-${ARCH_DIR}/lib/libsodium.so )
file(COPY ${PROJECT_SOURCE_DIR}/../libsodium/libsodium-android-${ARCH_DIR}/lib/libsodium.so DESTINATION "${PROJECT_SOURCE_DIR}/lib/${ANDROID_ABI}")

add_library(sodium-jni SHARED src/main/cpp/sodium-jni.c)
target_link_libraries(sodium-jni sodium)

2:Secondly Replace  node_modules/react-native-sodium/android/build.gradle
With this file
buildscript {
   repositories {
       google()
       jcenter()
   }
   dependencies {
       // classpath 'com.android.tools.build:gradle:1.3.1'
       classpath 'com.android.tools.build:gradle:3.4.2'
   }
}
 
apply plugin: 'com.android.library'
 
android {
   compileSdkVersion 28
   buildToolsVersion "28.0.3"
   defaultConfig {
       minSdkVersion 16
         compileSdkVersion 28
       targetSdkVersion 28
       versionCode 1
       versionName "0.1"
       externalNativeBuild {
           cmake {
               arguments "-DANDROID_TOOLCHAIN=clang"
           }
       }
       ndk {
           abiFilters 'x86', 'x86_64', 'armeabi-v7a', 'arm64-v8a'
       }
   }
   buildTypes {
       release {
           minifyEnabled false
           proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
       }
   }
   externalNativeBuild {
       cmake {
           path "CMakeLists.txt"
       }
   }
   sourceSets {
       main {
           jniLibs.srcDirs = ['./lib']
       }
   }
}
 
dependencies {
   compile fileTree(dir: 'libs', include: ['*.jar'])
   compile 'com.facebook.react:react-native:+'
 
   androidTestCompile('com.android.support.test.espresso:espresso-core:2.2.2', {
       exclude group: 'com.android.support', module: 'support-annotations'
   })
   testCompile 'junit:junit:4.12'
}
 
 
android {
 
}
 
dependencies {
}
 

